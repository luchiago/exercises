require 'time'

def calculate(array)
  array = array.sort
  minor = nil
  array.each_with_index do |element, index|
    if index != array.length - 1
      time1 = Time.parse(element)
      time2 = Time.parse(array[index + 1])
      difference = (time2 - time1)/60
      minor = difference if !minor || difference < minor
    end
  end
  time1 = Time.parse(array.first)
  time2 = Time.parse(array.last)
  difference = (time2 - time1)/60
  minor = difference if difference < minor
  return minor
end
p calculate(["23:59","00:00"])
p calculate(["03:12","01:00", "03:10"])

