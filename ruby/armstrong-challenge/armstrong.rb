# frozen_string_literal: true

class Armstrong
  def armstrong?(number)
    digits = divide(number, [])
    sum(digits, digits.length) == number
  end

  private

  def divide(number, array)
    return array if number <= 0

    array.append(number % 10)
    number /= 10
    divide(number, array)
  end

  def sum(digits, exp)
    digits.map { |d| d**exp }
          .sum
  end
end
