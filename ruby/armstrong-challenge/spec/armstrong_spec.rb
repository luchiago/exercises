# frozen_string_literal: true

require 'spec_helper'
require_relative '../armstrong.rb'

RSpec.describe 'Armstrong' do
  it 'Zero is an Armstrong number' do
    expect(Armstrong.new.armstrong?(0)).to eq(true)
  end

  it 'Single digit numbers are Armstrong numbers' do
    expect(Armstrong.new.armstrong?(5)).to eq(true)
  end

  it 'There are no 2 digit Armstrong numbers' do
    expect(Armstrong.new.armstrong?(10)).to eq(false)
  end

  it 'Three digit number that is an Armstrong number' do
    expect(Armstrong.new.armstrong?(153)).to eq(true)
  end

  it 'Three digit number that is not an Armstrong number' do
    expect(Armstrong.new.armstrong?(100)).to eq(false)
  end

  it 'Four digit number that is an Armstrong number' do
    expect(Armstrong.new.armstrong?(9474)).to eq(true)
  end

  it 'Four digit number that is not an Armstrong number' do
    expect(Armstrong.new.armstrong?(9475)).to eq(false)
  end

  it 'Seven digit number that is an Armstrong number' do
    expect(Armstrong.new.armstrong?(9_926_315)).to eq(true)
  end

  it 'Seven digit number that is not an Armstrong number' do
    expect(Armstrong.new.armstrong?(9_926_314)).to eq(false)
  end

  it 'Seventeen digit number that is an Armstrong number' do
    expect(Armstrong.new.armstrong?(21_897_142_587_612_075)).to eq(true)
  end
end
